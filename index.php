<?php 


require_once ('config/config.php');
require_once 'libs/Bootstrap.php';
require_once 'libs/Controller.php';
require_once ('classes/smarty/libs/Smarty.class.php');
require_once 'libs/Model.php';
require_once 'libs/Database.php';
require_once 'libs/Session.php';

//$smarty->display('header.tpl');


function __autoload($class){
    require_once 'libs/'.$class.'.php';
}

error_reporting(E_ERROR);
$app =  new Bootstrap();

?>