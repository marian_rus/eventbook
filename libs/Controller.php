<?php
//require_once ('http://eventbook.com/classes/smarty/libs/Smarty.class.php');
class Controller {

    function __construct() {

        //$this->view = new View();
        $this->smarty =  new Smarty();
        $this->smartySetup();
        Session::init();
    }

    function  loadModel($name){

        $path = 'models/'.$name.'_model.php';
        if(file_exists($path)){
            require 'models/'.$name.'_model.php';

            $modelName = $name .'_Model';
            $this->model=new $modelName();
        }
    }
    public function smartySetup()
    {

        $this->smarty->setTemplateDir('templates');
        $this->smarty->setCompileDir('libs/smarty/templates_c');
        $this->smarty->setCacheDir('libs/smarty/cache');
        $this->smarty->setConfigDir('libs/smarty/configs');


    }
}

?>
