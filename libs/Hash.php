<?php

class Hash {
/**
 * 
 * @param string $algo The algorithm
 * @param string $data The data to encode
 * @param string $salt
 * @return type The salted final hash
 */
    public static function create($algo, $data, $salt){
        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);
        return hash_final($context);
    }

}