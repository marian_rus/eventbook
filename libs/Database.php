<?php


class Database extends PDO {

    public function __construct(){
        $dsn = DB_TYPE.':'.DB_NAME.';'.DB_HOST;
        $user = DB_USER;
        $password = DB_PASS;

        parent::__construct($dsn, $user, $password);
    }


/**
 * select 
 * @param  string $sql
 * @param array $array parameters to bind
 * @param constant $fetchMode a PDO fetch Mode
 * @return mixed
 */
    public function select($sql, $array= array(), $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        foreach($array as $key => $value){
            $sth->bindValue(":$key", $value);
        }
        $sth->execute();
        return $sth->fetchAll($fetchMode);
    }
    
    
    /**
     * insert
     * @param string $table name of the table to insert into
     * @param type $data an associative array
     */
    public function insert($table, $data)
    {
        
      ksort($data);
      
       
        $fieldNames =implode('`,`', array_keys($data));
        $fieldValues = ':'.implode(', :', array_keys($data));
      
        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES($fieldValues) ");
         
         foreach ($data as $key => $value){
             $sth->bindValue(":$key", $value);
         }
         
        $sth->execute();
    }
    /**
     * update
     * @param string $table name of the table to insert into
     * @param type $data an associative array
     */
    public function update($table, $data, $where)
    {
         ksort($data);
      
       $fieldDetails = null;
         foreach($data as $key => $value){
             $fieldDetails .= "`$key`=:$key,";
         }
         $fieldDetails = rtrim($fieldDetails, ',');
       
         
        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
         
         foreach ($data as $key => $value){
             $sth->bindValue(":$key", $value);
         }
         
        $sth->execute();
    }

    /**
     * 
     * @param type $table
     * @param type $where
     * @param type $limit
     * @return integer
     */
    
        public function  delete($table, $where, $limit =1)
        {
          return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
             
        }
    
 }



?>
