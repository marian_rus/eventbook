<?php
class View {

    function __construct() {
      //  echo 'this is the view!!<br/>';
    }

    public function render($name, $noInclude = false) {
        if ($noInclude == true) {
            require 'templates/' . $name . '.tpl';
        } else {
            require 'templates/header.tpl';
            require 'templates/' . $name . '.tpl';
            require 'templates/footer.tpl';
        }
    
    }

}
?>
