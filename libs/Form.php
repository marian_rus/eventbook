<?php

/**
 * Fill out a form
 *  -Post to php 
 *  -sanitize
 *  -validate
 *  -return data
 *  -write tot db
 */
require 'Form/Val.php';
    class Form {

        /** @var  array $_currentItem the immediatly item*/
        private $_currentItem =  null;
        
        /** @var array $_PostData stores the posted data*/
        private $_postData =array();
      
        private $_val = array();
        
        /**
         *
         * @var type error
         */
        private $_error = array();
     /**
      * T
      */
        
     function __construct() {
         
         $this->_val = new Val();
    }
    /**
     * fetch -Return the posted data
     * @param mixed $fieldName
     * 
     * @return mixed string or an Array
     */
    public function fetch($fieldName =  false)
    {
        if($fieldName )
        {
            if(isset($this->_postData[$fieldName]))
            return $this->_postData[$fieldName]; 
        }else {
            return FALSE;
        }
        return $this->_postData;
    }
    
    /**
     * 
     * @this is to post
     */
    public function post($field){
      
        $this->_postData[$field]= $_POST[$field];
        $this->_currentItem = $field;
     
        return $this;
    }
    
    /**
     * this is to validate
     */
    
    public function val($typeOfValidator, $arg = null){
        
        if($arg == null){
            $error = $this->_val->{$typeOfValidator}($this->_postData[$this->_currentItem]);
        }else{
            $error = $this->_val->{$typeOfValidator}($this->_postData[$this->_currentItem], $arg);
        }
     
     
       if($error){
           $this->_error[$this->_currentItem]=$error;
       }
      
     
       return $this;
    }
    
    
    /**
     * submit handles the form and throws an exception upon error.
     * @return boolean
     * @throws Exception
     */
    public function  submit()
        {
            if(empty($this->_error)){
                return true;
            }else{
                $str = '';
                foreach ($this->_error as $key => $value){
                    $str .= $key . ' => '. $value ."\n";
                }
                throw new Exception($str);
            }
        }      
     

}
//15.15

?>
