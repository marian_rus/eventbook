<!doctype html>
<html>
<head>
    <title>Event Book</title>
    <link rel="stylesheet" href="{$URL}public/css/default.css"/>
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{$URL}public/js/custom.js"></script>



</head>

<body>
<div id="header">
 {if $smarty.session.loggedIn eq 'false'}
    <a href ="{$smarty.const.URL}index">Index</a>
    <a href ="{$smarty.const.URL}help">Help</a>
    <a href ="{$smarty.const.URL}login">Login</a>

        {elseif $smarty.session.role eq 'owner'}
        <a href="{$smarty.const.URL}user">Users</a>

        {else}
        <a href="{$smarty.const.URL}dashboard">Dashboard</a>
        <a href="{$smarty.const.URL}dashboard/newsFeed">Event Feeds</a>
        <a href="{$smarty.const.URL}note">Note</a>
        <a href="{$smarty.const.URL}dashboard/logout">Logout</a>
   {/if}



</div>


<div id="content">




