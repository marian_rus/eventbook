{include file="header.tpl"}

{assign var="module" value=$path.module}
{assign var="action" value=$path.action}

{if !isset($path.action) }
    {include file=  "$module/index.tpl"}
    {else}
    {include file=  "$module/$action.tpl"}
{/if}


{include file="footer.tpl"}



