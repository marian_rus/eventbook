<?php
class Dashboard extends Controller{

    function __construct() {
        parent::__construct();
        Auth::handleLogin();
        
        $this->view->js=array('dashboard/js/default.js');
    }

    function index(){
       
         $this->view->render('dashboard/index');
    }
    
    public function newsFeed()
    {
       $rss =  new rss();
        $this->view->render('dashboard/newsFeed');
         
    }
            
    function logout(){
        Session::destroy();
        header('location:'.URL.'login');
        exit;
    }
    
    function xhrInsert(){
        $this->model->xhrInsert();
    }
    
    function xhrGetListings(){
        $this->model->xhrGetListings();
    }//14.03
   
    function xhrDeleteListing(){
        $this->model->xhrDeleteListing();
    }
}